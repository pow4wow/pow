package pow

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	nonceTestValue     = "nonce"
	boundDataTestValue = "bound"
)

var (
	testRequest, testJobResult string
)

func TestNewProverWithBoundData(t *testing.T) {
	t.Run("OK", func(t *testing.T) {
		p, err := NewProverWithBoundData([]byte(boundDataTestValue))
		assert.NoError(t, err)
		assert.NotNil(t, p)
	})

	t.Run("MUST FAIL", func(t *testing.T) {
		p, err := NewProverWithBoundData([]byte(""))
		assert.Error(t, err)
		assert.Nil(t, p)
	})
}

func TestNewProverWithNonce(t *testing.T) {
	t.Run("OK", func(t *testing.T) {
		p, err := NewProverWithNonce([]byte(nonceTestValue))
		assert.NoError(t, err)
		assert.NotNil(t, p)
	})

	t.Run("MUST FAIL", func(t *testing.T) {
		p, err := NewProverWithNonce([]byte(""))
		assert.Error(t, err)
		assert.Nil(t, p)
	})
}

//
func TestPoWProver_Generate(t *testing.T) {
	t.Run("OK", func(t *testing.T) {
		prover := NewProver(WithNonce([]byte(nonceTestValue)))

		req, err := prover.Generate()
		assert.NoError(t, err)
		assert.NotEmpty(t, req)

		testRequest = req
	})

	t.Run("MUST FAIL", func(t *testing.T) {
		prover := NewProver(WithNonce([]byte("")))

		req, err := prover.Generate()
		assert.Error(t, err)
		assert.Empty(t, req)
	})
}

func TestPoWProver_DoWork(t *testing.T) {
	t.Run("OK", func(t *testing.T) {
		prover := NewProver(WithBoundData([]byte(boundDataTestValue)))

		result, err := prover.DoWork(testRequest)
		assert.NoError(t, err)
		assert.NotEmpty(t, result)

		testJobResult = result
	})

	t.Run("MUST FAIL: invalid prover", func(t *testing.T) {
		prover := NewProver(WithBoundData([]byte("")))

		result, err := prover.DoWork(testRequest)
		assert.Error(t, err)
		assert.Empty(t, result)
	})

	t.Run("MUST FAIL: invalid request", func(t *testing.T) {
		prover := NewProver(WithBoundData([]byte(boundDataTestValue)))

		result, err := prover.DoWork("invalid-request")
		assert.Error(t, err)
		assert.Empty(t, result)
	})
}

func TestPoWProver_Validate(t *testing.T) {
	t.Run("OK", func(t *testing.T) {
		prover := NewProver(WithRequest(testRequest), WithBoundData([]byte(boundDataTestValue)))

		err := prover.Validate(testJobResult)
		assert.NoError(t, err)
	})

	t.Run("MUST FAIL: invalid prover", func(t *testing.T) {
		prover := NewProver(WithBoundData([]byte("")))

		err := prover.Validate(testJobResult)
		assert.Error(t, err)
	})

	t.Run("MUST FAIL: invalid job result", func(t *testing.T) {
		prover := NewProver(WithRequest(testRequest), WithBoundData([]byte(boundDataTestValue)))

		err := prover.Validate("invalid-result")
		assert.Error(t, err)
	})
}
