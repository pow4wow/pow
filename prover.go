package pow

import (
	"github.com/bwesterb/go-pow"
)

const (
	proverDefaultDifficulty = 5
)

// Prover represents entity that should handle Proof of Work defence
type Prover interface {
	// Generate returns string with job task for prover
	// or error if it fails in some reasons
	Generate() (string, error)
	// Validate verifies if prover job is done properly
	Validate(proof string) error
	// DoWork should make all mandatory work on client side
	// and return string with proof, or error if it fails
	DoWork(req string) (string, error)
}

// PoWProver implements Prover interface with sha2bday algorithm
type PoWProver struct {
	difficulty uint32
	nonce      []byte
	boundData  []byte
	req        string
}

func (p *PoWProver) Generate() (string, error) {
	if len(p.nonce) == 0 {
		return "", ErrNonceMissing
	}

	req := pow.NewRequest(p.difficulty, p.nonce)
	p.req = req

	return req, nil
}

func (p *PoWProver) Validate(proof string) error {
	if len(p.boundData) == 0 {
		return ErrBoundDataMissing
	}

	ok, err := pow.Check(p.req, proof, p.boundData)
	if err != nil {
		return err
	}

	if !ok {
		return ErrInvalidProof
	}

	return nil
}

func (p *PoWProver) DoWork(req string) (string, error) {
	if len(p.boundData) == 0 {
		return "", ErrBoundDataMissing
	}

	return pow.Fulfil(req, p.boundData)
}

// ProverOption is a function to modify prover instance on create
type ProverOption func(prover *PoWProver)

// WithDifficulty is an option used to set up job difficulty
func WithDifficulty(d uint32) ProverOption {
	return func(p *PoWProver) {
		p.difficulty = d
	}
}

// WithNonce used to setup nonce value which will be used in job task generation
func WithNonce(nonce []byte) ProverOption {
	return func(p *PoWProver) {
		p.nonce = nonce
	}
}

// WithBoundData used to setup bound data which will be used in clients work
func WithBoundData(bound []byte) ProverOption {
	return func(p *PoWProver) {
		p.boundData = bound
	}
}

// WithRequest used to setup generated job request value
func WithRequest(req string) ProverOption {
	return func(p *PoWProver) {
		p.req = req
	}
}

// NewProver returns prover instance with applied options
// Do not use it without any option in arguments
func NewProver(options ...ProverOption) Prover {
	p := &PoWProver{
		difficulty: proverDefaultDifficulty,
	}

	applyOptions(p, options...)

	return p
}

// NewProverWithBoundData returns prover instance with applied bound data
// It will be helpful on client side
func NewProverWithBoundData(data []byte, options ...ProverOption) (Prover, error) {
	if len(data) == 0 {
		return nil, ErrBoundDataMissing
	}

	return NewProver(WithBoundData(data)), nil
}

// NewProverWithNonce returns prover instance with applied nonce value
// It will be helpful on server side, but most proper way is to use NewProver with options
func NewProverWithNonce(nonce []byte, options ...ProverOption) (Prover, error) {
	if len(nonce) == 0 {
		return nil, ErrNonceMissing
	}

	return NewProver(WithNonce(nonce)), nil
}

func applyOptions(p *PoWProver, opts ...ProverOption) {
	for _, option := range opts {
		option(p)
	}
}
