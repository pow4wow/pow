module gitlab.com/pow4wow/pow

go 1.16

require (
	github.com/bwesterb/go-pow v1.0.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0
)
