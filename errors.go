package pow

import "github.com/pkg/errors"

var (
	ErrInvalidProof     = errors.New("invalid proof value")
	ErrBoundDataMissing = errors.New("bound data is mandatory for prover instance, please use appropriate option in constructor")
	ErrNonceMissing     = errors.New("nonce data is mandatory for prover instance, please use appropriate option in constructor")
)
